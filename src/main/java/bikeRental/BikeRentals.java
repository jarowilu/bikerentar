package bikeRental;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BikeRentals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Jaką kwotą zasiliłeś swoje konto?");
        int money = scanner.nextInt();
        Wallet wallet = new Wallet(money);

        List<Feauters> listBike = Arrays.asList(
                new Feauters("Wigry", "biały", true, 8d, 1),
                new Feauters("Wigry", "czerwony", true, 15d, 2),
                new Feauters("Wigry", "niebieski", true, 12d, 3),
                new Feauters("Wigry", "zielony", true, 9d, 4),
                new Feauters("Wigry 3", "fioletowy", true, 18d, 5),
                new Feauters("Wigry 3", "biały", true, 12d, 6),
                new Feauters("Wigry 3", "czarny", false, 10d, 7),
                new Feauters("Wigry 3", "żółty", false, 10d, 8),
                new Feauters("Wigry 3", "szaryy", true, 10d, 9),
                new Feauters("Wigry 3", "brązowy", true, 8d, 10));

        System.out.println("masz do wyboru następujące rowery:\n ");
       listBike.stream().filter(Feauters -> Feauters.getPrice() <= money).forEach(System.out::println);

        System.out.println("wybierz 1 jeśli Cie interesuje marka roweru\nwybierz 2 jeśli szukasz koloru\nwybierz 3 jeśli chcesz zarezerwować rower");
        int brand = scanner.nextInt();
        switch (brand) {
            case 1:
                System.out.println("wpisz nazwe roweru");
                String name = scanner.next();

                listBike.stream().filter(Feauters -> Feauters.getBrand().equals(name) && Feauters.getPrice() <= money).forEach(System.out::println);
                break;
            case 2:
                System.out.println("wpisz kolor");
                String color = scanner.next();

                listBike.stream().filter(Feauters -> Feauters.getColor().equals(color) && Feauters.getPrice() <= money).forEach(System.out::println);
                break;
            case 3:
                System.out.println("podaj numer roweru, który chcesz zarezerwować: ");
                int numberBike = scanner.nextInt();
                listBike.stream().filter(Feauters-> Feauters.getNumber()==numberBike && Feauters.getPrice()<=money).forEach(System.out::println);
           break;
            default:
                System.out.println("nie ma takiej opcji");



        }
    }

    }


