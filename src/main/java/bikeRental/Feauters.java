package bikeRental;

public class Feauters {
private String brand;
private String color;
private boolean status;
private double price;
private int number;

    public Feauters(String brand, String color, boolean status, double price, int number) {
        this.brand = brand;
        this.color = color;
        this.status = status;
        this.price = price;
        this.number = number;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "" +
                "marka ='" + brand + '\'' +
                ", kolor='" + color + '\'' +
                ", dostępny=" + status +
                ", cena za godzine=" + price+ " numer roweru = "+ number;
    }
}

