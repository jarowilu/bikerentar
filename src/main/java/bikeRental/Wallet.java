package bikeRental;

public class Wallet {
    private int moneyStart; // tutaj przechowujemy startową kwotę posiadanych pieniędzy
    private int moneyEnd; // natomiast obrabiamy tę kwotę w trakcie doboru auta

    public Wallet(int moneyStart) {
        this.moneyStart = moneyStart;
        this.moneyEnd = moneyStart;
    }

    public int getMoneyStart() {
        return moneyStart;
    }

    public int getMoneyEnd() {
        return moneyEnd;
    }
}
